const express = require("express");
const app = express();

const hbs = require ('hbs');

const port = process.env.PORT || 3000

app.use(express.static(__dirname + '/public'))

//Express HBS engine
hbs.registerPartials(__dirname + '/views/partials')
app.set('view engine', 'hbs');

app.get("/", function(req, res){
    res.render('home', {
        nombre: 'Brucce',
        document: 'Pagina Principal',
        anio: new Date().getFullYear()
    })
});

app.get("/about", function(req, res){
    res.render('about', {
        nombre: 'Brucce',
        document: 'Acerca de',
        anio: new Date().getFullYear()
    })
});

app.get("/service", function(req, res){
    res.render('service', {
        nombre: 'Brucce',
        document: 'Servicios',
        anio: new Date().getFullYear()
    })
});

app.listen(port, () => console.log(`escuchando tus marranas peticiones desde el puerto ${port}`));